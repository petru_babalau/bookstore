from django.db import models
from django.contrib.auth.models import User
from django import utils
from django.core.validators import MaxValueValidator, MinValueValidator


class Purchase(models.Model):
	customer = models.ForeignKey(User)
	purchase_date = models.DateField(default=utils.timezone.now)
	rating = models.PositiveIntegerField(
		validators=[MaxValueValidator(5), MinValueValidator(0)],
		default=0,
	)

	def __str__(self):
		return '{} - {}'.format(self.customer, self.purchase_date)
