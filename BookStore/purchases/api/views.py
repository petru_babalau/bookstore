from rest_framework.generics import (
        CreateAPIView,
        UpdateAPIView,
        ListAPIView,
        RetrieveAPIView,
        DestroyAPIView,
    )

from rest_framework.permissions import (
        AllowAny,
        IsAuthenticated,
        IsAdminUser,
        IsAuthenticatedOrReadOnly,
    )

from .serializers import (
        PurchaseCreateSerializer,
        PurchaseUpdateSerializer,
        PurchaseListSerializer,
        PurchaseDetailsSerializer,
        PurchaseDeleteSerializer,
        PurchaseAddRatingSerializer,
    )
from rest_framework.response import Response
from rest_framework.status import (
        HTTP_200_OK, 
        HTTP_400_BAD_REQUEST,
        HTTP_204_NO_CONTENT,
    )
from ..models import Purchase


from cacheops import cache


class PurchaseListAPIView(ListAPIView):
    """
    Get a full list of orders.
    """
    permission_classes = [
        IsAuthenticated
    ]
    serializer_class = PurchaseListSerializer

    def get_queryset(self):
        queryset = Purchase.objects.filter(customer=self.request.user).cache(timeout=60 * 60)
        return queryset 


class PurchaseDetailsAPIView(RetrieveAPIView):
    """
    Get information about an order.
    """
    permission_classes = [
        IsAuthenticated
    ]
    serializer_class = PurchaseDetailsSerializer

    def get_queryset(self):
        queryset = Purchase.objects.filter(customer=self.request.user)
        return queryset 


class PurchaseDeleteAPIView(DestroyAPIView):
    """
    Delete an order.
    """
    permission_classes = [
        IsAuthenticated
    ]
    serializer_class = PurchaseDeleteSerializer

    def get_queryset(self):
        queryset = Purchase.objects.filter(customer=self.request.user)
        return queryset 


class PurchaseCreateAPIView(CreateAPIView):
    """
    Create a new order new order.
    """
    permission_classes = [
        IsAuthenticated
    ]
    serializer_class = PurchaseCreateSerializer
    # queryset = Purchase.objects.all()

    def post(self, request, *args, **kwargs):
        serializer = PurchaseCreateSerializer(data=request.data)

        if serializer.is_valid():
            #user_obj = self.request.user
            purchase_obj = Purchase(
                customer=self.request.user,
            )
            purchase_obj.save()
            return Response({"details": "Purchase created."}, status=HTTP_200_OK)
        return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)


class PurchaseUpdateAPIView(UpdateAPIView):
    """
    Update an order.
    """
    permission_classes = [
        IsAuthenticated
    ]
    serializer_class = PurchaseUpdateSerializer
    # queryset = Purchase.objects.all()

    def get_queryset(self):
        queryset = Purchase.objects.filter(customer=self.request.user)
        return queryset 


class PurchaseAddRatingAPIView(UpdateAPIView):
    """
    Add a rating to an order.
    """
    permission_classes = [
        IsAuthenticated
    ]
    serializer_class = PurchaseAddRatingSerializer

    def get_queryset(self):
        queryset = Purchase.objects.filter(customer=self.request.user)
        return queryset

    def update(self, request, *args, **kwargs):
        serializer = PurchaseAddRatingSerializer(data=request.data)
        if serializer.is_valid():
            purchase_qs = Purchase.objects.filter(pk=kwargs.get('pk'), customer=self.request.user)
            
            if not purchase_qs.exists():
                return Response({"details": "Purchase missing."}, status=HTTP_400_BAD_REQUEST)
            purchase_obj = purchase_qs.first()
            purchase_obj.rating = serializer.data.get('rating')
            purchase_obj.save()
            return Response({"details": "A rating was added."}, status=HTTP_200_OK)
        return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)





