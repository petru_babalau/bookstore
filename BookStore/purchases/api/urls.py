from django.conf.urls import url

from .views import (
		PurchaseListAPIView,
		PurchaseCreateAPIView,
		PurchaseUpdateAPIView, 
		PurchaseDetailsAPIView,
		PurchaseDeleteAPIView,
		PurchaseAddRatingAPIView,
	)

from purchases_items.api.views import (
		PurchaseItemListAPIView,
		PurchaseItemDeleteAPIView,
		PurchaseItemCreateAPIView,
	)


urlpatterns = [
	url(r'^$', PurchaseListAPIView.as_view(), name='purchase-list'),
	url(r'^create/', PurchaseCreateAPIView.as_view(), name='order-create'),
	url(r'^add/', PurchaseItemCreateAPIView.as_view(), name='add-new-item-line'),
	url(r'^add-rating/(?P<pk>\d+)/', PurchaseAddRatingAPIView.as_view(), name='order-add-rating'),
	url(r'^update/(?P<pk>\d+)/', PurchaseUpdateAPIView.as_view(), name='order-update'),
	#url(r'^add-item-line/', PurchaseAPIView.as_view(), name='order-add-item-line'),
	url(r'^delete/(?P<pk>\d+)/', PurchaseDeleteAPIView.as_view(), name='purchase-delete'),
	url(r'^items/(?P<pk>\d+)/', PurchaseItemListAPIView.as_view(), name='purchase-list-items'),
	url(r'^items/delete/(?P<pk>\d+)/', PurchaseItemDeleteAPIView.as_view(), name='purchase-item-delete'),
	url(r'^(?P<pk>\d+)', PurchaseDetailsAPIView.as_view(), name='purchase-details'),
]