from rest_framework.serializers import (
        StringRelatedField,
        HyperlinkedIdentityField,
        ModelSerializer,
        Serializer,
        SerializerMethodField,
        ValidationError,
        EmailField,
        CharField,
    )

from ..models import Purchase
from purchases_items.api.serializers import PurchaseItemDetailsSerializer


class PurchaseListSerializer(ModelSerializer):
    """
    Purchase List Serializer.
    api/purchases/
    """

    items = PurchaseItemDetailsSerializer(many=True)

    class Meta:
        model = Purchase
        fields = (
            'id',
            'customer',
            'purchase_date',
            'rating',
            'items',
        )


class PurchaseDetailsSerializer(ModelSerializer):
    items = PurchaseItemDetailsSerializer(many=True)

    class Meta:
        model = Purchase
        fields = (
            'id',
            'customer',
            'purchase_date',
            'rating',
            'items',
        )


class PurchaseDeleteSerializer(ModelSerializer):
    class Meta:
        model = Purchase
        fields = '__all__'


class MostPopularBooksSerializer(ModelSerializer):
    class Meta:
        model = Purchase
        fields = '__all__'


class PurchaseCreateSerializer(ModelSerializer):
    class Meta:
        model = Purchase
        exclude = (
            'customer',
            'purchase_date',
            'rating',
        )


class PurchaseUpdateSerializer(ModelSerializer):
    class Meta:
        model = Purchase
        fields = '__all__'


class PurchaseAddRatingSerializer(Serializer):
    #purchase = CharField(required=True)
    rating = CharField(required=True)

    def validate_rating(self, value):
        data = self.get_initial()
        rating = data.get("rating")
        
        try:
            rating = int(float(rating))
            if rating < 0 or rating > 5:
                raise ValidationError("Must be a number between 0 and 5.")
        except:
            raise ValidationError("Must be a number between 0 and 5.")
        return value



