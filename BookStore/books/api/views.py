from rest_framework.generics import (
        CreateAPIView,
        DestroyAPIView,
        ListAPIView,
        UpdateAPIView,
        RetrieveAPIView,
        RetrieveUpdateAPIView,
    )

from rest_framework.permissions import (
        AllowAny,
        IsAuthenticated,
        IsAdminUser,
        IsAuthenticatedOrReadOnly,
    )

from .serializers import (
        BookListSerializer,
        BookDetailsSerializer,
        BookFeedSerializer,
    )

from ..models import Book
from rest_framework import filters

from cacheops import file_cache


class BookListAPIView(ListAPIView):
    """
    Get a full list of books.
    """
    permission_classes = [AllowAny]
    serializer_class = BookListSerializer
    queryset = Book.objects.all()
    ordering_fields = (
        'title', 
        'author',
    )
    ordering = ('title',)
    filter_backends = (
        filters.SearchFilter,
        filters.OrderingFilter,
    )
    search_fields = ('title', 'author__name', 'author__surname')


class BookDetailsAPIView(RetrieveAPIView):
    """
    Get information about a book.
    """
    permission_classes = [
        AllowAny
    ]
    serializer_class = BookDetailsSerializer
    queryset = Book.objects.all()


class BookFeedListAPIView(ListAPIView):
    """
    Get a full feed of reated books.
    """
    permission_classes = [
        AllowAny
    ]
    serializer_class = BookFeedSerializer
    
    def get_queryset(self):
        return Book.objects.order_by('rating')

