from rest_framework.serializers import (
    StringRelatedField,
    HyperlinkedIdentityField,
    ModelSerializer,
    Serializer,
    SerializerMethodField,
    ValidationError,
    EmailField,
    CharField,
    )

from ..models import Book
from authors.api.serializers import AuthorDetailsSerializer


class BookListSerializer(ModelSerializer):
    """
    Book List Serializer.
    api/books/
    """

    author = AuthorDetailsSerializer(many=True)

    class Meta:
        model = Book
        fields = (
            'id',
            'title',
            'isbn',
            'publication_date',
            'author',
            'num_pages',
        )


class BookDetailsSerializer(ModelSerializer):
    class Meta:
        model = Book
        fields = '__all__'


class BookSimpleDetailsSerializer(ModelSerializer):
    author = StringRelatedField(many=True)
    class Meta:
        model = Book
        fields = (
            'title',
            'isbn',
            'author',
        )


class BookFeedSerializer(ModelSerializer):
    author = StringRelatedField(many=True)
    class Meta:
        model = Book
        fields = (
            'title',
            'author',
            'rating',
        )



