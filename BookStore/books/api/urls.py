from django.conf.urls import url

from .views import (
	BookListAPIView,
	BookDetailsAPIView,
	)
from purchases_items.api.views import (
	MostPurchasedItemsListAPIView,
)
from favorite_book_items.api.views import (
	MostFavoritesListAPIView,
)
from book_ratings.api.views import (
	MostRatedBookListAPIView,
)


urlpatterns = [
	url(r'^$', BookListAPIView.as_view(), name='book-list'),
	url(r'^(?P<pk>\d+)', BookDetailsAPIView.as_view(), name='details'),
]