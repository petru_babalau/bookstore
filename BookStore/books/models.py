from django.db import models
from authors.models import Author
from django.core.validators import MaxValueValidator, MinValueValidator


class Book(models.Model):
	title = models.CharField(max_length=100)
	isbn = models.CharField(max_length=20)
	author = models.ManyToManyField(Author, related_name="books")
	publication_date = models.DateField()
	num_pages = models.IntegerField(blank=True, null=True)

	def __str__(self):
		return "{} - {}".format(self.title, self.isbn)


	class Meta:
		ordering = (
			'title', 
		)