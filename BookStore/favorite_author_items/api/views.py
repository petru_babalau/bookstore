from rest_framework.generics import (
        CreateAPIView,
        ListAPIView,
    )
from rest_framework.permissions import (
        AllowAny,
        IsAuthenticated,
        IsAdminUser,
        IsAuthenticatedOrReadOnly,
    )
from .serializers import (
        FavoriteAuthorAddSerializer,
        FavoriteAuthorItemListSerializer,
    )
from rest_framework.response import Response
from rest_framework.status import (
        HTTP_200_OK, 
        HTTP_400_BAD_REQUEST,
        HTTP_204_NO_CONTENT,
    )

from ..models import FavoriteAuthorItem
from authors.models import Author


class FavoriteAuthorAddAPIView(CreateAPIView):
    """
    Add an author to favorite list of authors.
    """
    permission_classes = [IsAuthenticated]
    serializer_class = FavoriteAuthorAddSerializer

    def post(self, request, *args, **kwargs):
        serializer = FavoriteAuthorAddSerializer(data=request.data)

        if serializer.is_valid():
            author_id = serializer.data.get("author")
            user_obj = self.request.user

            favorite_item = FavoriteAuthorItem(
                user=user_obj,
                author=Author.objects.get(pk=author_id)
            )
            favorite_item.save()
            return Response({"details": "Author added to favorites."}, status=HTTP_200_OK)
        return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)


class FavoriteAuthorListAPIView(ListAPIView):
    """
    Get a list of favorite authors.
    """
    permission_classes = [IsAuthenticated]
    serializer_class = FavoriteAuthorItemListSerializer

    def get_queryset(self):
        user_obj = self.request.user
        queryset = FavoriteAuthorItem.objects.filter(user=user_obj)
        return queryset 

