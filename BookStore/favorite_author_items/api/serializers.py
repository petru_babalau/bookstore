from rest_framework.serializers import (
	    ModelSerializer,
    )
    
from ..models import FavoriteAuthorItem
from authors.api.serializers import AuthorSimpleDetailsSerializer


class FavoriteAuthorItemListSerializer(ModelSerializer):

	author = AuthorSimpleDetailsSerializer()

	class Meta:
	   	model = FavoriteAuthorItem
	   	fields = (
	   		'author',
	   	)


class FavoriteAuthorAddSerializer(ModelSerializer):
	class Meta:
		model = FavoriteAuthorItem
		fields = (
			'author',
		)
