from django.apps import AppConfig


class FavoriteAuthorItemsConfig(AppConfig):
    name = 'favorite_author_items'
