from django.db import models
from authors.models import Author
from django.contrib.auth.models import User


class FavoriteAuthorItem(models.Model):
	user = models.ForeignKey(User, related_name='favorite_authors', on_delete=models.CASCADE)
	author = models.ForeignKey(Author)

	def __str__(self):
		return '{} :: {}'.format(self.user, self.author)