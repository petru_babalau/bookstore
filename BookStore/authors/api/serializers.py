from rest_framework.serializers import (
    StringRelatedField,
    HyperlinkedIdentityField,
    ModelSerializer,
    Serializer,
    SerializerMethodField,
    ValidationError,
    EmailField,
    CharField,
    )

from ..models import Author


class AuthorListSerializer(ModelSerializer):
    """
    Author List Serializer.
    api/authors/
    """

    class Meta:
        model = Author
        fields = (
            'id',
            'full_name',
        )


class AuthorDetailsSerializer(ModelSerializer):
    class Meta:
        model = Author
        fields = '__all__'


class AuthorSimpleDetailsSerializer(ModelSerializer):
    class Meta:
        model = Author
        fields = (
            'full_name',
        )