from django.conf.urls import url

from .views import (
	AuthorListAPIView,
	AuthorDetailsAPIView,
	)


urlpatterns = [
	url(r'^$', AuthorListAPIView.as_view(), name='authors-list'),
	url(r'^(?P<pk>\d+)', AuthorDetailsAPIView.as_view(), name='details'),
]