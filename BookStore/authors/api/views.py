from rest_framework.generics import (
        CreateAPIView,
        DestroyAPIView,
        ListAPIView,
        UpdateAPIView,
        RetrieveAPIView,
        RetrieveUpdateAPIView,
    )

from rest_framework.permissions import (
        AllowAny,
        IsAuthenticated,
        IsAdminUser,
        IsAuthenticatedOrReadOnly,
    )

from .serializers import (
        AuthorListSerializer,
        AuthorDetailsSerializer,
    )

from ..models import Author


class AuthorListAPIView(ListAPIView):
    """
    Get a full list of authors
    """
    permission_classes = [AllowAny]
    serializer_class = AuthorListSerializer
    queryset = Author.objects.all()


class AuthorDetailsAPIView(RetrieveAPIView):
    """
    Get information about an author.
    """
    permission_classes = [
        AllowAny
    ]
    serializer_class = AuthorDetailsSerializer
    queryset = Author.objects.all()
