from django.db import models


class Author(models.Model):
	name = models.CharField(max_length=30)
	surname = models.CharField(max_length=30, blank=True)

	@property
	def full_name(self):
		return '{} {}'.format(self.name, self.surname)

	def __str__(self):
		return self.full_name

	class Meta:
		ordering = (
			'name', 
			'surname',
		)