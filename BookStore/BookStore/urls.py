"""BookStore URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from rest_framework import routers
from rest_framework_swagger.views import get_swagger_view
from rest_framework_jwt.views import obtain_jwt_token
from .admin import *
from book_ratings.api.views import (
    MostRatedBookListAPIView,
)


schema_view = get_swagger_view(title='BookStore API Documentation')


urlpatterns = [
	url(r'^admin/', admin.site.urls),
	url(r'^api/users/', include("accounts.api.urls", namespace='users-api')),
    url(r'^api/books/', include("books.api.urls", namespace='books-api')),
    url(r'^api/authors/', include("authors.api.urls", namespace='authors-api')),
    url(r'^api/purchases/', include("purchases.api.urls", namespace='purchases-api')),
    url(r'^api/feed/', MostRatedBookListAPIView.as_view(), name='feed'),
    url(r'api/token-auth/', obtain_jwt_token),
    url(r'docs/', schema_view),
]