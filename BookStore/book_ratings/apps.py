from django.apps import AppConfig


class BookRatingsConfig(AppConfig):
    name = 'book_ratings'
