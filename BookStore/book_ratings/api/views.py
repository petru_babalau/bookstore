from rest_framework.generics import (
		ListAPIView,
        CreateAPIView,
    )
from rest_framework.permissions import (
        AllowAny,
        IsAuthenticated,
        IsAdminUser,
        IsAuthenticatedOrReadOnly,
    )
from .serializers import (
	MostRatedBookListSerializer,
	BookRatingAddSerializer, 
    )
from rest_framework.response import Response
from rest_framework.status import (
        HTTP_200_OK, 
        HTTP_400_BAD_REQUEST,
        HTTP_204_NO_CONTENT,
    )
from django_filters.rest_framework import OrderingFilter
from rest_framework import filters
from books.models import Book
from django.db.models import Avg, Sum, Count

from ..models import BookRating


class MostRatedBookListAPIView(ListAPIView):
    """
    Get a list of most rated books.
    """
    permission_classes = [
        AllowAny,
    ]
    serializer_class = MostRatedBookListSerializer
    filter_backends = (filters.OrderingFilter,)
    ordering_fields = (
        'rating', 
        'title', 
        'author',
        'favorite',
        'items_sold',
    )
    ordering = ('rating', 'favorite')
    
    def get_queryset(self):
        # ----------------------------------------------------------------------------
        # WARNING: Multiple annotations give errors!!!
        # ----------------------------------------------------------------------------
        # queryset = Book.objects.annotate(
        #   rating=Avg('rating_set'), 
        #   favorite=Sum('favorite_set'),
        #   items_sold=Count('quantity_set')
        #)
        # ---------------------------------------------------------------------------

        queryset = Book.objects.extra(
            select={
                'items_sold': 'SELECT SUM(quantity) FROM purchases_items_purchaseitem WHERE '
                                 'books_book.id = purchases_items_purchaseitem.book_id',
                'rating': 'SELECT AVG(rating) FROM book_ratings_bookrating WHERE '
                          'books_book.id = book_ratings_bookrating.book_id',
                'favorite': 'SELECT COUNT(*) FROM favorite_book_items_favoritebookitem WHERE '
                            'books_book.id = favorite_book_items_favoritebookitem.book_id'
            }
        )

        return queryset


class BookRatingAddAPIView(CreateAPIView):
    """
    Add a rating of a book.
    """
    permission_classes = [IsAuthenticated]
    serializer_class = BookRatingAddSerializer

    def post(self, request, *args, **kwargs):
        serializer = BookRatingAddSerializer(data=request.data)
        print("POST")
        if serializer.is_valid():
            print(serializer.data)
            book_id = serializer.data.get("book")
            rating = serializer.data.get("rating")
            user_obj = self.request.user

            rating_item = BookRating(
                user=user_obj,
                book=Book.objects.get(pk=book_id),
                rating=rating
            )
            rating_item.save()
            return Response({"details": "A book was rated."}, status=HTTP_200_OK)
        return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)
