from rest_framework.serializers import (
        ModelSerializer,
        SerializerMethodField,
        IntegerField,
        Serializer,
        CharField,
        FloatField, 
    )
    
from ..models import BookRating
from books.models import Book
from django.db.models import Count  
from authors.api.serializers import AuthorSimpleDetailsSerializer


class BookRatingAddSerializer(Serializer):
    book = CharField(required=True)
    rating = CharField(required=True)

    def validate_rating(self, value):
        data = self.get_initial()
        rating = data.get("rating")
        
        try:
            rating = int(float(rating))
            if rating < 0 or rating > 5:
                raise ValidationError("Must be a number between 0 and 5.")
        except:
            raise ValidationError("Must be a number between 0 and 5.")
        return value

        

class MostRatedBookListSerializer(ModelSerializer):
    rating = FloatField(read_only=True)
    favorite = IntegerField(read_only=True)
    items_sold = IntegerField(read_only=True)
    author = AuthorSimpleDetailsSerializer(many=True)

    class Meta:
        model = Book
        fields = (
            'id',
            'title',
            'author',
            'rating',
            'favorite',
            'items_sold'
        )
