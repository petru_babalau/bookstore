from django.contrib import admin
from .models import BookRating

class BookRatingAdmin(admin.ModelAdmin):
    list_display = (
    	'book',
    	'user',
    	'rating',
    )


admin.site.register(BookRating, BookRatingAdmin)
