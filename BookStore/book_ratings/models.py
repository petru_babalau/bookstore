from django.db import models
from django.contrib.auth.models import User
from books.models import Book
from django.core.validators import MaxValueValidator, MinValueValidator


class BookRating(models.Model):
	user = models.ForeignKey(User, related_name='rating_books', on_delete=models.CASCADE)
	book = models.ForeignKey(Book, related_name='rating_set')
	rating = models.PositiveIntegerField(
		validators=[MaxValueValidator(5), MinValueValidator(0)],
		default=0,
	)

	def __str__(self):
		return '{} :: {}\nRating: {}'.format(self.user, self.book, self.rating)
