from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _
from django.db.models.signals import post_save


class UserProfile(models.Model):
    user = models.OneToOneField(User, related_name='details', on_delete=models.CASCADE)
    phone = models.CharField(max_length=20, blank=True, default='')
    address = models.CharField(max_length=100, blank=True, default='')

    def __str__(self):
        return self.phone
