from django.conf.urls import url
from .views import (
	UserListAPIView,
	UserCreateAPIView,
	UserChangePasswordAPIView,
	UserResetPasswordAPIView,
	UserLoginAPIView,
	UserUpdateAPIView,
	UserDetailsAPIView,
	)
from favorite_book_items.api.views import (
	FavoriteBookAddAPIView,
	FavoriteBookListAPIView,
	)
from favorite_author_items.api.views import (
	FavoriteAuthorAddAPIView,
	FavoriteAuthorListAPIView,
	)
from book_ratings.api.views import (
	BookRatingAddAPIView,
	)


urlpatterns = [
	url(r'^$', UserListAPIView.as_view(), name='users-list'),
	url(r'^register/$', UserCreateAPIView.as_view(), name='register'),
	url(r'^change-password/', UserChangePasswordAPIView.as_view(), name='change-password'),
	url(r'^login/$', UserLoginAPIView.as_view(), name='login'),
	url(r'^update/(?P<username>\w+)/$', UserUpdateAPIView.as_view(), name='update'),
	url(r'^reset-password/', UserResetPasswordAPIView.as_view(), name='reset-password'),
	url(r'^favorite-books/', FavoriteBookListAPIView.as_view(), name='list-favorite-books'),
	url(r'^add-favorite-book/', FavoriteBookAddAPIView.as_view(), name='add-favorite-book'),
	url(r'^favorite-authors/', FavoriteAuthorListAPIView.as_view(), name='list-favorite-authors'),
	url(r'^add-favorite-author/', FavoriteAuthorAddAPIView.as_view(), name='add-favorite-author'),
	url(r'^add-book-rating/', BookRatingAddAPIView.as_view(), name='add-book-rating'),
	# url(r'^buy/(?P<pk>\d+)', UserBuyBookAPIView.as_view(), name='buy-book'),
	# url(r'^(?P<pk>\d+)/delete/', UserDeleteAPIView.as_view(), name='delete'),
	# url(r'^(?P<pk>\d+)', UserDetailsAPIView.as_view(), name='details'),
]