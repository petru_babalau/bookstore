from django.db.models import Q

from rest_framework.serializers import (
    StringRelatedField,
    HyperlinkedIdentityField,
    ModelSerializer,
    Serializer,
    SerializerMethodField,
    ValidationError,
    EmailField,
    CharField,
    )

from django.contrib.auth.models import User
from django.contrib.auth.password_validation import validate_password
from ..models import UserProfile

from rest_framework_jwt.settings import api_settings
jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER
jwt_decode_handler = api_settings.JWT_DECODE_HANDLER
jwt_get_username_from_payload = api_settings.JWT_PAYLOAD_GET_USERNAME_HANDLER

from favorite_book_items.api.serializers import FavoriteBookItemListSerializer
from favorite_author_items.api.serializers import FavoriteAuthorItemListSerializer


class UserListSerializer(ModelSerializer):
    """
    User List Serializer.
    api/users/
    """
    # favorite_books = FavoriteBookItemListSerializer(many=True)
    # favorite_authors = FavoriteAuthorItemListSerializer(many=True)

    class Meta:
        model = User
        fields = (
            'id',
            'username',
            'first_name',
            'last_name',
            'email',
            # 'favorite_books',
            # 'favorite_authors',
        )


class UserCreateSerializer(ModelSerializer):
    """
    User Register Serializer
    api/users/register/
    """
    
    class Meta:
        model = User
        fields = [
            'username',
            'email',
            'password',
        ]
        extra_kwargs = {
            "password": {"write_only": True}
        }

    def validate(self, data):
        email = data['email']
        username = data['username']
        
        user_qs = User.objects.filter(username=username)
        if user_qs.exists():
            raise ValidationError("This username has already registered.")

        user_qs = User.objects.filter(email=email)
        if user_qs.exists():
            raise ValidationError("A user with this email has already registered.")
        
        return data 

    def create(self, validated_data):
        username = validated_data['username']
        email = validated_data['email']
        password = validated_data['password']
        user_obj = User(
                username=username,
                email=email,
                # phone=phone,
            )
        user_obj.set_password(password)
        user_obj.save()
        return validated_data


class UserChangePasswordSerializer(Serializer):
    """
    User Serializer for password change endpoint.
    """
    old_password = CharField(required=True)
    new_password = CharField(required=True)


def create_token(user):
    """
    Create token with JWT.
    """
    jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
    jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER
    payload = jwt_payload_handler(user)
    token = jwt_encode_handler(payload)
    return token


class UserLoginSerializer(ModelSerializer):
    """
    User serializer for login endpoint.
    api/users/login/
    """
    token = CharField(allow_blank=True, read_only=True)
    username = CharField(required=False, allow_blank=True)
    email = EmailField(label="Email Address", required=False, allow_blank=True)

    class Meta:
        model = User
        fields = [
            'username',
            'email',
            'password',
            'token',
        ]
        extra_kwargs = {
            "password": {"write_only": True}
        }

    def validate(self, data):
        email = data.get('email', None)
        username = data.get('username', None)
        password = data['password']
        user_obj = None
        if not email and not username:
            raise ValidationError('A username/email is required.')

        user = User.objects.filter(
                Q(username=username) |
                Q(email=email)
            ).distinct()
        user.exclude(email__isnull=True).exclude(email__iexact='')
        if user.exists() and user.count() == 1:
            user_obj = user.first()
        else:
            raise ValidationError("This username/email is not valid.")

        if user_obj:
            if not user_obj.check_password(password):
                raise ValidationError("Incorrect credentials.")

        data["token"] = create_token(user_obj)
        return data


class UserUpdateSerializer(ModelSerializer):    
    """
    User serializer for update user profile details.
    api/users/update/
    """
    class Meta:
        model = User
        fields = [
            'username',
            'first_name',
            'last_name',
            'email',
        ]


class UserDetailsSerializer(ModelSerializer):

    favorite_books = FavoriteBookItemListSerializer(many=True)
    favorite_authors = FavoriteAuthorItemListSerializer(many=True)


    class Meta:
        model = User
        fields = (
            'id', 
            'username',
            'first_name',
            'last_name',
            'email',
            'favorite_books',
            'favorite_authors',
        )


class UserResetPasswordSerializer(Serializer):
    """
    User Serializer for password reset endpoint.
    """
    email = CharField(required=True)

    def validate(self, data):
        email = data['email']

        user_qs = User.objects.filter(email=email)
        if not user_qs.exists():
            raise ValidationError("No users with this email is registered.")
        
        return data 


class UserBuyBookSerializer(ModelSerializer):
    pass



