from django.db.models import Q
from django.contrib.auth import get_user_model
from rest_framework.filters import (
        SearchFilter,
        OrderingFilter,
    )
from rest_framework.mixins import DestroyModelMixin, UpdateModelMixin
from rest_framework.generics import (
        CreateAPIView,
        DestroyAPIView,
        ListAPIView,
        UpdateAPIView,
        RetrieveAPIView,
        RetrieveUpdateAPIView,
    )
from rest_framework.views import APIView
from rest_framework.permissions import (
        AllowAny,
        IsAuthenticated,
        IsAdminUser,
        IsAuthenticatedOrReadOnly,
    )
from .serializers import (
        UserListSerializer,
        UserCreateSerializer,
        UserChangePasswordSerializer,
        UserResetPasswordSerializer,
        UserLoginSerializer,
        UserUpdateSerializer,
        UserDetailsSerializer,
    )
from rest_framework.response import Response
from rest_framework.status import (
        HTTP_200_OK, 
        HTTP_400_BAD_REQUEST,
        HTTP_204_NO_CONTENT,
    )
from rest_framework import permissions
from django.core import mail


User = get_user_model()


class IsOwnerOrReadOnly(permissions.BasePermission):
    """
    Custom permission to only allow owners of an object to edit it.
    """

    def has_object_permission(self, request, view, obj):
        # Read permissions are allowed to any request,
        # so we'll always allow GET, HEAD or OPTIONS requests.
        if request.method in permissions.SAFE_METHODS:
            return True

        # Write permissions are only allowed to the owner of the snippet.
        return obj.owner == request.user


class UserListAPIView(ListAPIView):
    """
    Get a full list of users.
    """
    permission_classes = [IsAuthenticated, ]
    serializer_class = UserListSerializer
    queryset = User.objects.all()


class UserCreateAPIView(CreateAPIView):
    """
    Endpoint for registering a new user.
    """
    permission_classes = [AllowAny]
    serializer_class = UserCreateSerializer
    queryset = User.objects.all()


def generate_password():
    import random, string
    return ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(8))


class UserResetPasswordAPIView(APIView):
    """
    Endpoint for reseting the password.
    """
    permission_classes = (
        AllowAny,
    )

    def put(self, request, *args, **kwargs):
        print("*** PUT ***")
        serializer = UserResetPasswordSerializer(data=request.data)

        if serializer.is_valid():
            email = serializer.data.get("email")
            user_obj = User.objects.filter(email=email).first()

            generated_password = generate_password()

            user_obj.set_password(generated_password)
            user_obj.save()

            with mail.get_connection() as connection:
                mail.EmailMessage(
                    '[BookStore] - Reset Password', 
                    'Your new password: {}'.format(generated_password), 
                    'support@bookstore.com', 
                    [email], 
                    connection=connection,
                    ).send()

            return Response(
                {   "details": "An email containing the new password was sent.",
                }, status=HTTP_200_OK)

        return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)


class UserChangePasswordAPIView(APIView):
    """
    Endpoint for changing password.
    """
    permission_classes = (
        #AllowAny,
        IsAuthenticated, 
    )

    def get_object(self, queryset=None):
        return self.request.user

    def put(self, request, *args, **kwargs):
        self.object = self.get_object()

        if not self.object.is_authenticated():
            Response({"details": ["User is not authenticated."]}, status=HTTP_400_BAD_REQUEST)
        serializer = UserChangePasswordSerializer(data=request.data)
            
        if serializer.is_valid():
            old_password = serializer.data.get("old_password")
            
            if not self.object.check_password(old_password):
                return Response({"old_password": ["Wrong password."]}, status=HTTP_400_BAD_REQUEST)
            
            self.object.set_password(serializer.data.get("new_password"))
            self.object.save()
            
            return Response(status=HTTP_200_OK)
        return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)


class UserLoginAPIView(APIView):
    """
    An endpoint for logging in.
    """ 
    permission_classes = [
        AllowAny,
    ]
    serializer_class = UserLoginSerializer

    def post(self, request, *args, **kwargs):
        serializer = UserLoginSerializer(data=request.data)

        if serializer.is_valid(raise_exception=True):
            new_data = serializer.data
            return Response(new_data, status=HTTP_200_OK)
        return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)

from rest_framework import permissions
class UserIsOwnerOrReadOnly(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        if request.method in permissions.SAFE_METHODS:
            return True
        return obj.id == request.user.id


class UserUpdateAPIView(UpdateAPIView):
    """
    Endpoint for updating user profile details.
    """
    permission_classes = [
        IsAuthenticated,
        UserIsOwnerOrReadOnly,
        ]
    serializer_class = UserUpdateSerializer
    lookup_field = 'username'
    queryset = User.objects.all()

def put(self, request, *args, **kwargs):
    self.object = self.get_object()

    if not self.object.is_authenticated():
        Response({"details": ["User is not authenticated."]}, status=HTTP_400_BAD_REQUEST)
    serializer = UserUpdateSerializer(data=request.data)
        
    if serializer.is_valid():
        username = serializer.data.get("username")
        first_name = serializer.data.get("first_name")
        last_name = serializer.data.get("last_name")
        email = serializer.data.get("email")
        
        self.object.first_name = first_name
        self.object.last_name = last_name
        self.object.email = email
        self.object.save()
        
        return Response(status=HTTP_200_OK)
    return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)


class UserDetailsAPIView(RetrieveAPIView):
    """
    Get information about an user.
    """
    permission_classes = [
        IsAuthenticated,
        # IsOwnerOrReadOnly, 
    ]
    serializer_class = UserDetailsSerializer
    queryset = User.objects.all()























