# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2017-11-08 14:50
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('favorite_book_items', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='favoritebookitem',
            name='book',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='favorite', to='books.Book'),
        ),
    ]
