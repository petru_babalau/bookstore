from django.contrib import admin
from .models import FavoriteBookItem


admin.site.register(FavoriteBookItem)