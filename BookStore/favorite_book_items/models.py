from django.db import models
from books.models import Book
from django.contrib.auth.models import User


class FavoriteBookItem(models.Model):
	user = models.ForeignKey(User, related_name='favorite_books', on_delete=models.CASCADE)
	book = models.ForeignKey(Book, related_name='favorite_set')

	def __str__(self):
		return '{} :: {}'.format(self.user, self.book)