from rest_framework.generics import (
        CreateAPIView,
        ListAPIView,
    )
from rest_framework.permissions import (
        AllowAny,
        IsAuthenticated,
        IsAdminUser,
        IsAuthenticatedOrReadOnly,
    )
from .serializers import (
        FavoriteBookAddSerializer,
        FavoriteBookItemListSerializer,
        MostFavoritesBooksListSerializer, 
    )
from rest_framework.response import Response
from rest_framework.status import (
        HTTP_200_OK, 
        HTTP_400_BAD_REQUEST,
        HTTP_204_NO_CONTENT,
    )
from ..models import FavoriteBookItem
from books.models import Book
from rest_framework import viewsets
from rest_framework import filters
from django.db.models import Count


class FavoriteBookAddAPIView(CreateAPIView):
    """
    Add a book to favorite books list.
    """
    permission_classes = [IsAuthenticated]
    serializer_class = FavoriteBookAddSerializer

    def post(self, request, *args, **kwargs):
        serializer = FavoriteBookAddSerializer(data=request.data)

        if serializer.is_valid():
            book_id = serializer.data.get("book")
            user_obj = self.request.user

            favorite_item = FavoriteBookItem(
                user=user_obj,
                book=Book.objects.get(pk=book_id)
            )
            favorite_item.save()
            return Response({"details": "Book added to favorites."}, status=HTTP_200_OK)
        return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)


class FavoriteBookListAPIView(ListAPIView):
    """
    Get a list of favorite books.
    """
    permission_classes = [IsAuthenticated]
    serializer_class = FavoriteBookItemListSerializer

    def get_queryset(self):
        user_obj = self.request.user
        queryset = FavoriteBookItem.objects.filter(user=user_obj)
        return queryset 


class MostFavoritesListAPIView(ListAPIView):
    """
    Get a list of most favorite books.
    """
    permission_classes = [
        AllowAny,
    ]
    serializer_class = MostFavoritesBooksListSerializer
    filter_backends = (filters.OrderingFilter,)
    ordering_fields = ('favorite', )
    ordering = ('favorite',)

    def get_queryset(self):
        queryset = Book.objects.annotate(favorite=Count('favorite_set'))
        return queryset




