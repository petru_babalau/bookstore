from rest_framework.serializers import (
        ModelSerializer,
        SerializerMethodField,
        IntegerField,
    )
    
from ..models import FavoriteBookItem
from books.api.serializers import BookSimpleDetailsSerializer
from books.models import Book
from django.db.models import Count  
from authors.api.serializers import AuthorSimpleDetailsSerializer


class FavoriteBookItemListSerializer(ModelSerializer):

    book = BookSimpleDetailsSerializer()

    class Meta:
        model = FavoriteBookItem
        fields = (
            'book',
        )


class FavoriteBookAddSerializer(ModelSerializer):
    class Meta:
        model = FavoriteBookItem
        fields = (
            'book',
        )


class MostFavoritesBooksListSerializer(ModelSerializer):
    favorite = IntegerField(read_only=True)
    author = AuthorSimpleDetailsSerializer(many=True)

    class Meta:
        model = Book
        fields = (
            'id',
            'title',
            'author',
            'favorite',
        )

