from django.apps import AppConfig


class FavoriteBookItemsConfig(AppConfig):
    name = 'favorite_book_items'
