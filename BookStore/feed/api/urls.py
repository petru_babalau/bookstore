from django.conf.urls import url
from books.api.views import BookFeedListAPIView


urlpatterns = [
	url(r'^$', BookFeedListAPIView.as_view(), name='feed-book-list'),
]