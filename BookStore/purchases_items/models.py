from django.db import models
from books.models import Book
from purchases.models import Purchase
from django.core.validators import MaxValueValidator, MinValueValidator


class PurchaseItem(models.Model):
	purchase = models.ForeignKey(Purchase, related_name='items', on_delete=models.CASCADE)
	book = models.ForeignKey(Book, related_name='quantity_set')
	quantity = models.PositiveIntegerField(
		validators=[MaxValueValidator(100), MinValueValidator(1)],
		default=1,
	)

	def __str__(self):
		return '{} :: {} ({} buc.)'.format(self.purchase, self.book, self.quantity)