from django.contrib import admin
from .models import PurchaseItem


admin.site.register(PurchaseItem)
