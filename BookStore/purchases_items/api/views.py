from rest_framework.generics import (
		ListAPIView,
        CreateAPIView,
        DestroyAPIView,
    )

from rest_framework.permissions import (
        AllowAny,
        IsAuthenticated,
        IsAdminUser,
        IsAuthenticatedOrReadOnly,
    )

from .serializers import (
        PurchaseItemListSerializer,
        PurchaseItemCreateSerializer,
        PurchaseItemDeleteSerializer,
        MostPurchasedItemsListSerializer,
    )
from rest_framework.response import Response
from rest_framework.status import (
        HTTP_200_OK, 
        HTTP_400_BAD_REQUEST,
        HTTP_204_NO_CONTENT,
    )
from django_filters.rest_framework import DjangoFilterBackend, OrderingFilter
from rest_framework import filters

from ..models import PurchaseItem
from purchases.models import Purchase
from books.models import Book
from django.db.models import Sum


class PurchaseItemCreateAPIView(CreateAPIView):
    """
    Add a book to order.
    """
    permission_classes = [IsAuthenticated]
    serializer_class = PurchaseItemCreateSerializer


    def post(self, request, *args, **kwargs):
        serializer = PurchaseItemCreateSerializer(data=request.data)

        if serializer.is_valid():
            purchase_id = serializer.data.get('purchase')
            book_id = serializer.data.get('book')
            
            book_qs = Book.objects.filter(pk=book_id)
            if not book_qs.exists():
                return Response({"details": "Book missing, mf."}, status=HTTP_400_BAD_REQUEST)

            purchase_qs = Purchase.objects.filter(pk=purchase_id, customer=self.request.user)
            if not purchase_qs.exists():
                return Response({"details": "Book can not be added to this purchase."}, status=HTTP_400_BAD_REQUEST)

            purchase_obj = purchase_qs.first()
            book_obj = book_qs.first()
            purchase_item = PurchaseItem(
                purchase=purchase_obj,
                book=book_obj,
                quantity=serializer.data.get('quantity')
            )
            purchase_item.save()
            return Response({"details": "Book added to the purchase."}, status=HTTP_200_OK)
        return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)


class PurchaseItemListAPIView(ListAPIView):
    """
    Get a list of books from an order.
    """
    permission_classes = [IsAuthenticated]
    serializer_class = PurchaseItemListSerializer
    queryset = PurchaseItem.objects.all()


class PurchaseItemDeleteAPIView(DestroyAPIView):
    """
    Delete a book from an order.
    """
    permission_classes = [IsAuthenticated]
    serializer_class = PurchaseItemDeleteSerializer
    queryset = PurchaseItem.objects.all()


class MostPurchasedItemsListAPIView(ListAPIView):
    """
    Get a list of most purchased books.
    """
    permission_classes = [
        AllowAny,
    ]
    serializer_class = MostPurchasedItemsListSerializer
    filter_backends = (filters.OrderingFilter,)
    ordering_fields = (
        'quantity', 
        'title', 
        'author',
    )
    ordering = ('quantity', )

    def get_queryset(self):
        queryset = Book.objects.annotate(quantity=Sum('quantity_set'))
        return queryset

