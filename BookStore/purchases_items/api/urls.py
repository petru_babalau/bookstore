'''
from django.conf.urls import url

from .views import (
		PurchaseItemCreateAPIView,
		MostPurchasedItemsListAPIView,
	)


urlpatterns = [
	url(r'^$', MostPurchasedItemsListAPIView.as_view(), name='most-purchased-items-list'),
	url(r'^add/', PurchaseItemCreateAPIView.as_view(), name='add-new-item-line'),
]
'''