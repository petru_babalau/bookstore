from rest_framework.serializers import (
        ModelSerializer,
        SerializerMethodField,
        StringRelatedField
    )

from ..models import PurchaseItem
from purchases.models import Purchase
from books.api.serializers import BookSimpleDetailsSerializer

from books.models import Book
from django.db.models import Sum
from authors.api.serializers import AuthorSimpleDetailsSerializer

class PurchaseItemCreateSerializer(ModelSerializer):
    class Meta:
        model = PurchaseItem
        fields = (
            'quantity',
            'book',
            'purchase',
        )


class PurchaseItemDetailsSerializer(ModelSerializer):
    book = BookSimpleDetailsSerializer()

    class Meta:
        model = PurchaseItem
        fields = (
            'book',
            'quantity',
        )


class PurchaseItemListSerializer(ModelSerializer):
    book = BookSimpleDetailsSerializer()

    class Meta:
        model = PurchaseItem
        fields = (
            'id',
            'book',
            'quantity',
        )


class PurchaseItemDeleteSerializer(ModelSerializer):
    class Meta:
        model = PurchaseItem
        fields = '__all__'


class MostPurchasedItemsListSerializer(ModelSerializer):
    quantity = SerializerMethodField()
    author = AuthorSimpleDetailsSerializer(many=True)

    class Meta:
        model = Book
        fields = (
            'id',
            'title',
            'author',
            'quantity',
        )

    def get_quantity(self, obj):
        purchased = PurchaseItem.objects.filter(book=obj).values('book_id').annotate(quantity=Sum('quantity')).order_by('quantity')    
        return purchased.values('quantity').first()['quantity']

