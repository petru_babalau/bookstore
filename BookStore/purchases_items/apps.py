from django.apps import AppConfig


class PurchasesItemsConfig(AppConfig):
    name = 'purchases_items'
